#include "userprog/syscall.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "threads/synch.h"
#include "filesys/filesys.h"
#include "filesys/file.h"
#include "devices/shutdown.h"
#include "threads/vaddr.h"

static struct lock file_lock;
static void syscall_handler (struct intr_frame *);
bool create(const char *file, unsigned initial_size);
bool remove(const char *name);
int open(const char *name);
void exit(int status);
int wait (pid_t pid);
pid_t exec (const char *cmd_line);
void exit (int status);
void halt (void);
int  read (int fd, void *buffer, unsigned size);
int write(int fd, const void *buffer, unsigned size);
struct file * map_file(int fd);
unsigned tell (int fd);
int filesize (int fd );
void seek (int fd , unsigned position );
void close (int fd );
struct child_info * get_child(tid_t id,struct thread * curr);

void
syscall_init (void) 
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
  lock_init(&file_lock);
}

/* syscall_handler sequence:
  1 - Fetch the stack pointer
  2 - Check that it's valid stack pointer--> validate that it's user stack pointer
  3 - Fetch the system call number (Top of stack)
  4 - Call the system call method that needs for this number
*/
static void
syscall_handler (struct intr_frame *f UNUSED) 
{
  /* fetch the stack pointer */
  void *sp = f->esp;
  validate(sp);
  //printf("fofofofoofofof%d\n",sp);


  /* fetch sys_call number  and check its vaild*/
  int sys_call_num = (int) (*(int *)sp);
  if (sys_call_num < SYS_HALT || sys_call_num > SYS_INUMBER)
          exit(-1);

  //printf("syscall number %d %d\n",sys_call_num , SYS_WRITE);


  /*argument pointers --> max equal 3 */
  void *arg_0;
  void *arg_1;
  void *arg_2;
    /* Call the system call method that needs for this sys_call_num */
    switch(sys_call_num)
    {
         case SYS_HALT:
           halt();
          break;
        
        case SYS_EXIT:
            get_argument(sp,1, &arg_0, &arg_1, &arg_2);
            exit((int)arg_0);
          break;

        case SYS_EXEC:
            get_argument(sp,1, &arg_0, &arg_1, &arg_2);
             f->eax = (uint32_t)exec((const char*)arg_0);
        break;  

          case SYS_WAIT:
            get_argument(sp,1, &arg_0, &arg_1, &arg_2);
             f->eax = (uint32_t)wait( (pid_t)arg_0);
          break;

         case SYS_CREATE:
            get_argument(sp,2, &arg_0, &arg_1, &arg_2);
             f->eax = (uint32_t)create( (const char*)arg_0, (unsigned) arg_1 );
          break;
         
         case SYS_REMOVE:
            get_argument(sp,1, &arg_0, &arg_1, &arg_2);
            f->eax = (uint32_t) remove( (const char *) arg_0);
         break;

         case SYS_OPEN:
             get_argument(sp,1, &arg_0, &arg_1, &arg_2);
              f->eax = (uint32_t)open( (const char *) arg_0);
         break;

         case SYS_WRITE:
             get_argument(sp,3, &arg_0, &arg_1, &arg_2);
             f->eax = (uint32_t)write( (int) arg_0, (const void *) arg_1, (unsigned)arg_2);
         break;

         case SYS_READ:
             get_argument(sp,3, &arg_0, &arg_1, &arg_2);
             f->eax = (uint32_t)read( (int) arg_0, (const void *) arg_1, (unsigned)arg_2);
         break;

         case SYS_TELL:
              get_argument(sp,1, &arg_0, &arg_1, &arg_2);
              f->eax = (uint32_t)tell ((int) arg_0);
         break;

         case SYS_FILESIZE:
             get_argument(sp,1, &arg_0, &arg_1, &arg_2);
             f->eax = (uint32_t)filesize( (int) arg_0);
         break;

         case SYS_SEEK:
             get_argument(sp,2, &arg_0, &arg_1, &arg_2);
             seek( (int) arg_0,  (unsigned)arg_1);
         break;

         case SYS_CLOSE:
             get_argument(sp,1, &arg_0, &arg_1, &arg_2);
             close( (int) arg_0);
         break;


    }

}




/* validate the stack pointer */
void
validate(void *sp)
{


  /* check if sp is null --> if yes exit */
    if(sp == NULL)
        exit(-1);

    /* check sp below PHYS_BASE --> if no exit */
    if (!is_user_vaddr(sp) )
         exit(-1);

     /* check if page directory of current thread 
      * is mapped to non null kernel virtual address*/
     uint32_t *pd = thread_current()->pagedir;
     void* kernel_virtual_address = pagedir_get_page (pd, sp);
     
     if (kernel_virtual_address == NULL)
      exit(-1);

}

/* get the argument */
void
get_argument(void* sp , int argument_num , void** arg_0, void ** arg_1 , void ** arg_2)
{
  /* point to first arg and validate it */
  sp += 4;
  validate(sp);

    if(argument_num > 0)
    {
        *arg_0 = *(void **)sp;
        sp += 4;
        validate(sp);

    }

    if(argument_num > 1)
    {
        *arg_1 = *(void **)sp;
        sp += 4;
        validate(sp);
    }

    if(argument_num > 2)
    {
        *arg_2 = *(void **)sp;

    }



}

/* create file */
bool
create(const char *file, unsigned initial_size)
{
   validate(file);
   lock_acquire(&file_lock);
    bool creat_success = filesys_create (file, initial_size);
    lock_release(&file_lock);
    return creat_success;
}

/* remove file */
bool
remove(const char *name)
{
   validate(name);
   lock_acquire(&file_lock);
    bool remove_success = filesys_remove(name);
    lock_release(&file_lock);
    return remove_success;
}

int
open(const char *name)
{
   if(name == NULL)
     return -1;
    
    /* Check if file name is in user address space */
    validate(name);

    lock_acquire(&file_lock);
    struct file *opened_file =filesys_open (name);
    lock_release(&file_lock);

    if(opened_file == NULL)
        return -1;

    /*assign file descriptor and name*/
    opened_file->fd = thread_current()->fd_count++; 
    /* push file to process's open_file_list */
    list_push_back(&thread_current()->open_file, &opened_file->open_file_elem);
    return opened_file->fd;
}


/*halt process*/
void halt (void)
{
    shutdown_power_off();
}
/*exit process*/
void exit (int status) 
{
    char * command = thread_name() ;
    char * save_ptr ;
    char * thread_name =  strtok_r (command, " \t", &save_ptr);
    printf ("%s: exit(%d)\n",thread_name , status);
    thread_current()->info->exit_status = status;
    thread_current()->info->user_exit = true;
    thread_exit();
}

/*execute the process*/
pid_t exec (const char *cmd_line)
{
    validate(cmd_line);
   
    pid_t pid = (pid_t)process_execute(cmd_line);
    /*faild to create the child thread*/
    if(pid == -1 )
    {
         return -1;
    }
      
    /*parent has to wait for the child to load*/ 

    sema_down(&thread_current()->load );

    if(thread_current()->child_load_status == true)
    {
       thread_current()->child_load_status = -1;
       return pid;
    }else{// child faild to load & free its resources from the parent list
       
      struct child_info * child = get_child(pid,thread_current());
      if (child!= NULL)
      {
        list_remove(&child->child_elem);
        free(child);
      }
    }
      return -1;
}
/*wait for child process to exit*/  
int wait (pid_t pid)
{
  return process_wait(pid);

}

int
write(int fd, const void *buffer, unsigned size)
{

  validate(buffer);
  lock_acquire(&file_lock);
  if(fd == 1)
  {
    putbuf (buffer, size);
    lock_release(&file_lock);
    return size;
  }
  struct file * mapped_file = map_file(fd);
  if(mapped_file == NULL)
  {
    lock_release(&file_lock);
     exit(-1);
  }

   
   int writen_byte = (int)file_write (mapped_file, buffer, size) ;
   lock_release(&file_lock);
   
   return writen_byte;


}

int 
read (int fd, void *buffer, unsigned size) 
{

   validate(buffer);
   
   lock_acquire ( &file_lock);
   int read_size = 0 ;
  if(fd == 0 )
      {
          for(read_size = 0 ; read_size < size ; read_size++)
          {
              *(uint8_t * )buffer = input_getc();  
              buffer++;
          }
          lock_release (&file_lock);
          return read_size ;
      }

   struct file *mapped_file = map_file(fd);
   if(mapped_file == NULL)
   {
      lock_release (&file_lock);
      exit(-1);
   }
   
    
    read_size = (int) file_read(mapped_file , buffer ,size);
    lock_release (&file_lock);
    return read_size;
  

      
      // check for input from keyboard
      
      
    //  return -1;  
}

struct file *
map_file(int fd)
{
  struct list_elem *elem;
  for (elem = list_begin (&thread_current()->open_file); elem != list_end (&thread_current()->open_file); elem = list_next (elem))
    {
      struct file *mapped_file = list_entry (elem, struct file,open_file_elem);
      if(mapped_file->fd == fd)
      {
        return mapped_file;
      }
    } 
    return NULL;
}

unsigned
tell (int fd)
{
  lock_acquire(&file_lock);
  struct file * mapped_file = map_file(fd);
  if(mapped_file == NULL)
  {
    lock_release(&file_lock);
     exit(-1); 
  }

   
   unsigned returned_pointer = (unsigned)file_tell(mapped_file);
   lock_release(&file_lock);
   return returned_pointer;  

}
void close (int fd )
{
   if(fd<0)
   {
      exit(-1);
   }
   lock_acquire ( &file_lock);
   struct file *mapped_file = map_file(fd);
   if(mapped_file == NULL)
   {
     lock_release ( &file_lock );
     exit(-1);
   }
   
   list_remove (&mapped_file->open_file_elem);
   file_close (mapped_file) ;
   lock_release ( &file_lock );
}
void seek (int fd , unsigned position )
{
   if(fd<0 || position<0)
      exit(-1);
    lock_acquire ( &file_lock);
   struct file *file=map_file(fd);
   if(file==NULL)
   {
      lock_release ( &file_lock );
      exit(-1);
   }
  
   file_seek (file, (unsigned)position);
   lock_release ( &file_lock );
}

int filesize (int fd )
{
   if(fd<0)
   {
      exit(-1);
   }
   lock_acquire ( &file_lock);
   struct file *file=map_file(fd);
   if(file==NULL)
   {
      lock_release ( &file_lock );
      exit(-1);
   }

    int size=(int)file_length (file) ;
    lock_release ( &file_lock );
   
   return size;
}

struct child_info * get_child(tid_t id,struct thread * curr)
{
  struct list_elem * e;
  for (e=list_begin(&curr->children);
    e!=list_end(&curr->children);e=list_next(e))
  {
    struct child_info * child = list_entry(e,struct child_info ,child_elem);
    if(child->child_tid == id)
      return child;
  }
  return NULL;
}



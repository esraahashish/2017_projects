#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <dirent.h>
#define MAX_LINE 80 /* The maximum length command */
#define MAX_HISTORY 10

int split_command(char *args[], char command[] , int command_lenght);
void handle_sigchld(int sig);
void  processCommand(char command[]);
void readFile(char *argv);
void addToHistory(char command[]);
int isValidNum(char num[]);
int perfomHistoryCommand(int historyIndex);
int split(char * input , char * splitChar , char * splittedArray[]);
char * getCommandPath(char * commandName);
int getSystemPaths();
void viewHistory();
void saveHistoryToFile();
void readHistoryFromFile();
char *history[MAX_HISTORY] = {"/0"};
char *systemPaths [100] = {"/0"};
int historyIndex = 0 ;
int numberSystemPaths = 0;

int main(int argc, char **argv)
{
    signal( SIGCHLD ,handle_sigchld);
    readHistoryFromFile();
    numberSystemPaths = getSystemPaths();

    // check if normal mode or batch mode
    if(argv[1] != NULL)
    {
        printf("**********batch mode enable**********\n");
        readFile(argv[1]);
    }
    else
    {
        char *command = malloc(MAX_LINE*MAX_LINE);
        while (1)
        {
            printf("shell>");
            fflush(stdout);

            if (fgets (command, MAX_LINE*MAX_LINE, stdin) == NULL)
            {
                saveHistoryToFile();
                exit(EXIT_SUCCESS);
            }

            processCommand(command);

        }
        free(command);
    }
    return 0;
}

void  processCommand(char command[])
{
    char *args[MAX_LINE/2 + 1] = {"/0"};
    char *commandPath = malloc(MAX_LINE*MAX_LINE) ;
    int commandLength = 0;
    int numberOfArgs = 0;
    pid_t pid ;
    pid_t wait_pid ;
    int foreground = 0 ;
    commandLength = strlen(command)-1;


    // check for max len error of command
    if(commandLength > MAX_LINE)
    {
        printf("**ERROR : Command has exceeded max Length!**\n");
        addToHistory(command);
        return;
    }
    else if (commandLength == 0)
    {
        printf("**ERROR : Command is Empty!**\n");
        return;
    }

    numberOfArgs =  split_command(args, (command) , commandLength);

    // check for max number of arguments
    if(numberOfArgs >= MAX_LINE/2)
    {
        printf("**ERROR : Arguments have exceeded max Value**\n");
        addToHistory(command);
        return;
    }else if(numberOfArgs == 0){
        printf("**ERROR : NO command exist!**\n");
        return;
    }


    args[numberOfArgs] = NULL ;
    int lengthOfLastArgument = strlen(args[numberOfArgs-1]) -1;
    //ls  &
    if(strcmp( args[numberOfArgs-1] , "&" ) == 0 )
    {
        foreground = 0;
        args[numberOfArgs-1] = NULL;
    }// ls&
    else if(args[numberOfArgs-1][lengthOfLastArgument] == '&'){
         foreground = 0 ;
         args[numberOfArgs-1][lengthOfLastArgument] = 0;
         args[numberOfArgs-1][lengthOfLastArgument+1] = 0;
    }
    else{//ls
        foreground = 1;

    }


    // special user commands(exit , history , !#)
    if(strcmp(args[0],"exit") == 0 && numberOfArgs == 1)
    {
        saveHistoryToFile();
        exit(EXIT_SUCCESS);
    }
    else if(strcmp(args[0],"history") == 0 && numberOfArgs == 1)
    {
        printf("**The History :**\n");
        viewHistory();
        return;
    }
    else if(numberOfArgs == 1 && args[0][0] == '!' && isValidNum(strdup(args[0])))
    {
        return;
    }

    /*add the current command to history*/
    addToHistory(command);

    commandPath = getCommandPath(args[0]);

    if(commandPath != NULL)
    {

        pid = fork();
        if(pid < 0)   /*error has occured*/
        {
            printf("**ERROR : fork faild**\n");
            return;
        }
        else if(pid == 0)     /*child process*/
        {
            printf("**********In child process %d**********\n",getpid());
            perror(execv(commandPath,args));
            exit(EXIT_FAILURE);
        }
        else if(pid > 0)     /*parent process*/
        {
            if(foreground)   /*foreground mode*/
            {
                wait_pid = wait(NULL);
                printf("**********Child completed its process %d**********\n",wait_pid);
            }/*else background mode */
            return ;
        }
    }
    else
    {
        printf("**ERROR : command '%s' not found in PATH variables**\n",args[0]);
    }

    return ;

}


//read arguments from file line by line
void readFile(char *argv)
{
    FILE * fp;
    char * line = malloc(MAX_LINE*MAX_LINE);
    size_t len = 0;
    ssize_t read;

    fp = fopen(argv , "r");

    if (fp == NULL)
    {
        printf("**ERROR : Can't open the required file**\n ");
        exit(EXIT_FAILURE);
    }

    while ((read = getline(&line, &len, fp)) != -1)
    {
        printf("**Retrieved command from file : %s**\n" , line);
        processCommand(line);
    }

    fclose(fp);
    if (line)
        free(line);
    exit(EXIT_SUCCESS);
}

// add commands to history
void addToHistory(char command[])
{
    if(historyIndex == MAX_HISTORY)
    {
        int i;
        historyIndex--;
        for(i=1 ; i < MAX_HISTORY ; i++)
        {
            history[i-1] = history[i];
        }
    }
    // to overcome memory reference
    history[historyIndex++] = strdup(command);
}

// list commands in history
void viewHistory()
{
    int i;
    if(historyIndex == 0)
    {
        printf("**Error : History is Empty !**\n");
        return;
    }
    for(i = historyIndex ; i > 0 ; i--)
    {
        printf("%d- %s\n", i , history[i-1]);
    }
}

void saveHistoryToFile()
{
    FILE *fp;
    fp=fopen("history.txt", "w");
    if(fp == NULL)
    {
        printf("**ERROR :failed to save to history file **\n");
    }
    else
    {
        int itr ;
        for(itr = 0 ; itr < historyIndex ; itr++)
        {
            fprintf(fp, "%s",history[itr]);
        }
        fclose(fp);
    }
}

void readHistoryFromFile()
{
    FILE * fp;
    char * line = malloc(MAX_LINE*MAX_LINE);
    size_t len = 0;
    ssize_t read;

    fp = fopen("history.txt" , "r");

    if (fp == NULL)
    {
        printf("**Error : No History Retrieved **\n");
    }
    else
    {

        while ((read = getline(&line, &len, fp)) != -1)
        {
            history[historyIndex++] = strdup(line) ;
        }
        fclose(fp);
        if (line)
            free(line);
    }

}

/*check for correct syntax of history commands call
 * return zero for syntax error
 *return -1 for history non matching index
 *return 1 for success history access
 */
int isValidNum(char num[])
{
    //remove first ! in the command
    memmove(&num[0], &num[0 + 1], strlen(num));

    if(strlen(num)==NULL)
    {
        return 0;
    }
    else if(strlen(num) == 1 && num[0] == '!')
    {
        return perfomHistoryCommand(historyIndex-1);
    }
    else if(atoi(num) != 0)
    {
        return perfomHistoryCommand(atoi(num)-1);
    }else if( strcmp(num , "0") == 0)
    {
        return perfomHistoryCommand(-1);
    }
    else
    {
        return 0 ;
    }
}

// perform commands from history
int perfomHistoryCommand(int index)
{

    if(index < 0 ||  index >= historyIndex || historyIndex == 0)
    {
        printf("**ERROR : History doesnt include this index **\n");
        return -1;
    }
    else
    {
        // check for white space command
        printf("%s\n",history[index]);
        processCommand(history[index]);
        return 1 ;
    }
}

/* return the path of the commad*/
char * getCommandPath(char * commandName )
{
    // /bin/cp
    if(!access(commandName, F_OK))
    {
        return commandName;
    }

    int itr ;
    char * directory = malloc(MAX_LINE*MAX_LINE);
    for(itr = 0 ; itr < numberSystemPaths ; itr++ )
    {
        asprintf(&directory,"%s%s%s",systemPaths[itr],"/",commandName);
        int check = access(directory, F_OK);

        if(check == 0)
        {
            return directory;
        }

    }
    free(directory);

    return NULL;
}


// split Input on the required char
int split(char * input , char * splitChar , char * splittedArray[])
{
    char * token = malloc(MAX_LINE*MAX_LINE);
    int index = 0 ;
    token = strtok (input,splitChar);

    while (token != NULL)
    {
        splittedArray[index++] = token ;
        token = strtok (NULL, splitChar);
    }
    return index;
}

int getSystemPaths()
{
    char * paths = getenv("PATH");
    return split(paths , ":\n" , systemPaths);
}


void handle_sigchld(int sig)
{
    pid_t pid;
    int status;
    while (1)
    {
        /* -1 ---> wait for any child process ,  status information of the terminated proces , WNOHANG -->not blocking */
        pid = waitpid(-1, &status, WNOHANG);
        if ( pid <= 0)
            /* No more zombie children to reap. */
            break;
        /* NOT REENTRANT!  Avoid in practice! */
        printf("**********REAPED Child%d**********\n",pid);

    }
    sleep(1);
}


int split_command(char *args[], char command[] , int command_length)
{
    int args_index = 0;
    int scan_itr = 0;
    int flag =  0 ;
    char *argument = malloc(sizeof(char) * MAX_LINE);

    for(int i = 0; i < command_length ; i++)
    {

        if(command[i] == '\"')
        {
            flag = 1;
            int j;
            for( j = i+1 ; j < command_length && command[j] != '\"' ; j++ )
            {
                argument[scan_itr++] = command[j];
            }
            if(command[j] != '\"'){
               printf("Warning: not complete String!\n");
            }
                i = j ;
        }
        else if (command[i] != '\t' && command[i] != ' ')
        {
            flag = 1 ;
            int j;
            for(j = i; j < command_length && command[j] != '\t' && command[j] != ' ' ; j++)
            {
                argument[scan_itr++] =command[j];

            }
            i=j;
        }
        if(flag){
            argument[scan_itr++] = '\0';
            args[args_index++] = argument;
            scan_itr = 0;
            argument = malloc(sizeof(char) * MAX_LINE);
            flag = 0 ;
        }
    }
    free(argument);
    return args_index;
}


